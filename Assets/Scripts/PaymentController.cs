﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class PaymentController : MonoBehaviour
{
    public Animator stateMachine;
    public MarketplaceItemController marketplaceItem;

    public bool IsCredit { set { _isCredit = value; } }
    bool _isCredit;

    int res = -1;
    string hostRespCode = String.Empty;

    struct PaymentParams
    {
        public string type;
        public string commPort;
        public string priceInCents;
    };

    [DllImport("posnetDLL")]
    private static extern uint DebitPurchase(string commPort, string priceInCents, out string hostRespCode);

    [DllImport("posnetDLL")]
    private static extern uint CreditPurchase(string commPort, string priceInCents, out string hostRespCode);

    // Use this for initialization
    void OnEnable ()
    {
        StartCoroutine(PaymentCoroutine());
    }

    void MakePurchase(object tparams)
    {
        PaymentParams p = (PaymentParams)tparams;
        switch (p.type)
        {
            case "credit":
                res = (int)CreditPurchase(p.commPort, p.priceInCents, out hostRespCode);
                break;
            case "debit":
                res = (int)DebitPurchase(p.commPort, p.priceInCents, out hostRespCode);
                break;
            default:
                break;
        }
    }

    IEnumerator PaymentCoroutine ()
    {
        yield return new WaitForSeconds(.48f);

        Thread t = new Thread(MakePurchase);
        var iniFile = new IniFile.IniFile(Application.dataPath + "/settings.ini");
        float total = marketplaceItem.TotalPrice * 100;

        t.IsBackground = true;
        t.Priority = System.Threading.ThreadPriority.Lowest;
        t.Start(new PaymentParams()
        {
            commPort = iniFile.GetValue("Posnet", "ComPort", "COM4"),
            priceInCents = total.ToString(),
            type = (_isCredit) ? "credit" : "debit"
        });

        while (res == -1) yield return new WaitForSeconds(.48f);

        Debug.Log("PaymentController::PaymentCoroutine - MakePurchase: " + res);
        Debug.Log("PaymentController::PaymentCoroutine - hostRespCode: " + hostRespCode);

        if (res == 0 && hostRespCode == "00")
        {
            stateMachine.SetTrigger("nextStep");
        }
        else
        {
            stateMachine.SetTrigger("error");
        }

        res = -1;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
