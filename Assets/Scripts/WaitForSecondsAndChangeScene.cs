﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitForSecondsAndChangeScene : MonoBehaviour
{
    public float seconds;
    public int sceneId;
    public SceneController _sceneController;

    void OnEnable()
    {
        StartCoroutine(WaitForSecondsAndGotoNextStep());
    }

    void OnDisable()
    {
        StopCoroutine(WaitForSecondsAndGotoNextStep());
    }

    IEnumerator WaitForSecondsAndGotoNextStep()
    {
        yield return new WaitForSeconds(seconds);
        gameObject.SetActive(false);
        _sceneController.ChangeScene(sceneId);
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
