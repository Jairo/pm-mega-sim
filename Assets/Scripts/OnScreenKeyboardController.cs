﻿using UnityEngine;
using UnityEngine.UI;

public class OnScreenKeyboardController : MonoBehaviour
{
    //public InputField InputText { set { _inputField = value; } }
    public InputField InputText;

    int caretPosition;

    public void SendCharToInput(char key)
    {
        if (InputText != null)
        {
            if (InputText.text.Length > 0)
                InputText.text = InputText.text.Substring(0, caretPosition) + key + InputText.text.Substring(caretPosition);
            else
                InputText.text = key.ToString();

            caretPosition++;
        }
    }

    public void Backspace()
    {
        if (InputText.text.Length > 0 && caretPosition > 0)
        {
            InputText.text = InputText.text.Substring(0, caretPosition - 1) + InputText.text.Substring(caretPosition);
            caretPosition = Mathf.Max(0, caretPosition - 1);
        }
    }

    public void SetCaretPosition()
    {
        caretPosition = Mathf.Max(0, InputText.caretPosition);
    }
}
