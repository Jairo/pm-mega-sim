﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[Serializable]
public class Item
{
    public string id;
    public string name;
}

[Serializable]
public class ItemList
{
    public Item[] List;
}

[Serializable]
public class DNIList
{
    public long[] List;
}

public class AutocompleteBehaviour : MonoBehaviour
{
    public string JSONUrl;
    public Dropdown _dropdown;
    public bool isLinked;
    public bool isOnPersistentFolder;
    public AutocompleteBehaviour linkedDropdown;
    public bool DropdownOpened { set { dropdownOpened = value; } }

    Item[] _itemList;
    private InputField _inputField;
    bool dropdownOpened = false;

    void Awake()
    {
        _inputField = GetComponent<InputField>();
    }

    // Use this for initialization
    void Start()
    {
        if (!isLinked) LoadOptions();
    }

    // Update is called once per frame
    void Update()
    {

    }

    List<Item> Find(string searchString)
    {
        return new List<Item>(_itemList.Where(a => a.name.ToLower().Contains(searchString.ToLower())));
    }

    string GetJSON()
    {
        string json = "";
        if (isOnPersistentFolder)
        {
            if (File.Exists(Path.Combine(Application.persistentDataPath, JSONUrl + ".json")))
                json = File.ReadAllText(Path.Combine(Application.persistentDataPath, JSONUrl + ".json"));
        }
        else
            json = Resources.Load<TextAsset>((!isLinked) ? JSONUrl : JSONUrl + linkedDropdown._inputField.text).text;

        return json;
    }

    public void LoadOptions()
    {
        _itemList = JsonUtility.FromJson<ItemList>(GetJSON()).List;
        RefreshOptions(new List<Item>(_itemList));
    }

    void RefreshOptions(List<Item> itemList)
    {
        _dropdown.ClearOptions();
        foreach (Item item in itemList)
        {
            Dropdown.OptionData option = new Dropdown.OptionData
            {
                text = item.name
            };
            _dropdown.options.Add(option);
        }
    }

    public void DropdownShow()
    {
        //if (force && _inputField.text.Length < 3)
        //{
        //    _dropdown.ClearOptions();
        //}

        if (!dropdownOpened)
        {
            dropdownOpened = true;
            _dropdown.Show();
        }
    }

    public void SelectFirstItem()
    {
        _dropdown.value = 0;
        SetText();
        //if (_dropdown.options.Count > 1) _dropdown.value = 0;
    }

    public void SetText()
    {
        _inputField.text = _dropdown.options[_dropdown.value].text;
    }

    public void SetLinkedDropdown(AutocompleteBehaviour dropdown)
    {
        linkedDropdown = dropdown;
    }

    public void SuggestOptions()
    {
        RefreshOptions(Find(_inputField.text));
        _dropdown.Hide();
        if (Find(_inputField.text).Count == 1) _dropdown.value = 1;
        else _dropdown.value = -1;
    }
}
