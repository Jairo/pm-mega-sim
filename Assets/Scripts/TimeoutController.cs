﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(AppModelController))]
public class TimeoutController : MonoBehaviour
{
    public string triggerName;

    float timeoutInSeconds;
    float timeoutOperationMaxDelay;
    Animator _animator;
    AppModelController appModelController;

    // Use this for initialization
    void Start()
    {
        IniFile.IniFile iniFile = new IniFile.IniFile(Application.dataPath + "/settings.ini");
        timeoutInSeconds = (float)iniFile.GetDouble("Timeout", "timeoutInSeconds", 3);
        timeoutOperationMaxDelay = (float)iniFile.GetDouble("Timeout", "timeoutOperationMaxDelay", 3);

        _animator = GetComponent<Animator>();
        appModelController = GetComponent<AppModelController>();
    }

    public void StartTimeout()
    {
        StartCoroutine(StartTimeoutCoroutine());
    }

    public void StopTimeout()
    {
        StopAllCoroutines();
    }

    public void RestartTimeout()
    {
        StopTimeout();
        StartTimeout();
    }

    IEnumerator StartTimeoutCoroutine()
    {
        yield return new WaitForSeconds(timeoutInSeconds);
        _animator.SetTrigger(triggerName);
        yield return new WaitForSeconds(timeoutOperationMaxDelay);
        appModelController.SaveModelOnline(true);

    }
}