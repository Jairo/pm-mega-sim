﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

[RequireComponent(typeof(SceneController))]
public class AppModelController : MonoBehaviour
{
    public string registerFilename;

    SceneController _sceneController;

    // Use this for initialization
    void Start()
    {
        Debug.Log(Path.Combine(Application.persistentDataPath, registerFilename));
        _sceneController = GetComponent<SceneController>();
    }

    public void SaveModelOnline(bool cancelled)
    {
        if (AppModel._user.buyOnly || AppModel._user.dni != 0)
        {
            if (cancelled) AppModel._ventas = new List<Venta>();
            StartCoroutine(SaveModel(cancelled));
        }
        else
        {
            AppModel.Clear(); // This shouldn't be necessary
            _sceneController.ChangeScene(0);
        }
    }

    void SaveRegistry(AppModelInterface model)
    {
        File.AppendAllText(Path.Combine(Application.persistentDataPath, registerFilename),
        (File.Exists(Path.Combine(Application.persistentDataPath, registerFilename)) ? ",\n" : "") + JsonUtility.ToJson(model, true));

        if (AppModel._user.dni != 0)
        {
            List<long> dniList = (File.Exists(Path.Combine(Application.persistentDataPath, "dni.json")))
                               ? new List<long>(JsonUtility.FromJson<DNIList>(File.ReadAllText(Path.Combine(Application.persistentDataPath, "dni.json"))).List)
                               : new List<long>();
            dniList.Add(AppModel._user.dni);

            File.WriteAllText(Path.Combine(Application.persistentDataPath, "dni.json"), JsonUtility.ToJson(new DNIList() { List = dniList.ToArray() }), Encoding.UTF8);
        }
        AppModel.Clear();
    }

    IEnumerator SaveModel(bool cancelled)
    {
        Dictionary<string, string> postHeader = new Dictionary<string, string>
        {
            { "Content-Type", "application/json" }
        };

        IniFile.IniFile iniFile = new IniFile.IniFile(Application.dataPath + "/settings.ini");
        AppModelInterface model = new AppModelInterface(AppModel._operator, AppModel._user, AppModel._ventas);
        var formData = Encoding.UTF8.GetBytes(JsonUtility.ToJson(model));
        using (WWW req = new WWW("http://" + iniFile.GetValue("Server", "ip") + "/register", formData, postHeader))
        {
            yield return req;
            Debug.Log(Encoding.UTF8.GetString(req.bytes));
            SaveRegistry(model);
            if (cancelled) _sceneController.ChangeScene(0);
        }
    }
}
