﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

[RequireComponent(typeof(InputField))]
public class ScannerBehaviour : MonoBehaviour
{
    public Animator stateMachine;
    public char delimiter;

    InputField _inputField;
    bool isProcessing = false;

    // Use this for initialization
    void Awake()
    {
        _inputField = GetComponent<InputField>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!_inputField.isFocused) _inputField.Select();
    }
    public void ProcessScan()
    {
        if (!isProcessing)
        {
            Regex regex = new Regex(BuildRegEx((_inputField.text.StartsWith(delimiter.ToString())) ? 8 : 7));
            if (regex.IsMatch(_inputField.text))
            {
                isProcessing = true;
                DNI userData = DeserializeDNI(_inputField.text);
                Debug.Log("ScannerBehaviour::Scan - userData = {dni: " + userData.dni + ", nombre: " + userData.name + ", apellido: " + userData.lastName + ", fecha de nacimiento: " + userData.birthdate + ", sexo: " + userData.gender + "}");
                DateTime birthDate = DateTime.ParseExact(userData.birthdate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                if (birthDate.AddYears(18) <= DateTime.Today)
                {
                    if (File.Exists(Path.Combine(Application.persistentDataPath, "dni.json")))
                    {
                        long[] dniList = JsonUtility.FromJson<DNIList>(File.ReadAllText(Path.Combine(Application.persistentDataPath, "dni.json"))).List;
                        List<long> dniQuery = new List<long>(dniList.Where(dni => dni.Equals(userData.dni)));
                        if (dniQuery.Count > 0)
                        {
                            MakeShortRegistry(userData.gender, birthDate);
                            stateMachine.SetBool("registrado", true);
                            stateMachine.SetTrigger("nextStep");
                        }
                        else
                        {
                            StartCoroutine(ValidateDNI(userData, birthDate));
                        }
                    }
                    else
                    {
                        StartCoroutine(ValidateDNI(userData, birthDate));
                    }
                }
                else
                {
                    isProcessing = false;
                    stateMachine.SetTrigger("menor");
                }
            }
        }
    }

    string BuildRegEx(int numberOfFields)
    {
        string returnVal = "";
        for (int i = 0; i < numberOfFields; ++i) returnVal += "(.*?)\\" + delimiter;
        return returnVal;
    }

    IEnumerator ValidateDNI(DNI userData, DateTime birthDate)
    {
        var iniFile = new IniFile.IniFile(Application.dataPath + "/settings.ini");

        AppModel._user.name = userData.name;
        AppModel._user.lastName = userData.lastName;
        AppModel._user.dni = userData.dni;
        AppModel._user.birthdate = birthDate.ToString("yyyy-MM-dd");
        AppModel._user.gender = userData.gender;

        using (WWW req = new WWW("http://" + iniFile.GetValue("Server", "ip") + "/dni?dni=" + userData.dni.ToString()))
        {
            yield return req;
            string result = Encoding.UTF8.GetString(req.bytes);
            if (result != "" && JsonUtility.FromJson<DNIResult>(result).dni > 0)
            {
                MakeShortRegistry(userData.gender, birthDate);
                stateMachine.SetBool("registrado", true);
            }
            stateMachine.SetTrigger("nextStep");
        }
        isProcessing = false;
    }

    void MakeShortRegistry(string gender, DateTime birthDate)
    {
        int ageRange;
        if (birthDate.AddYears(34) <= DateTime.Today)
        {
            ageRange = 3;
        }
        else if (birthDate.AddYears(29) <= DateTime.Today)
        {
            ageRange = 2;
        }
        else if (birthDate.AddYears(24) <= DateTime.Today)
        {
            ageRange = 1;
        }
        else
        {
            ageRange = 4;
        }

        AppModel._user = new Perfil()
        {
            buyOnly = true,
            gender = gender,
            rangeage = ageRange
        };
    }

    DNI DeserializeDNI(string barCodeValue)
    {
        Debug.Log("ScannerBehaviour::DeserializeDNI barCodeValue - " + barCodeValue);
        int[,] dataOrder = new int[,] { { 1, 2, 4, 3, 6 }, { 4, 5, 1, 8, 7 } };
        int useOrder = (barCodeValue.StartsWith(delimiter.ToString())) ? 1 : 0;
        string[] value = barCodeValue.Split(delimiter);

        return new DNI
        {
            dni = Int64.Parse(value[dataOrder[useOrder, 2]]),
            name = value[dataOrder[useOrder, 1]],
            lastName = value[dataOrder[useOrder, 0]],
            birthdate = value[dataOrder[useOrder, 4]],
            gender = value[dataOrder[useOrder, 3]]
        };
    }
}

class DNIResult
{
    public int dni;
}

struct DNI
{
    public long dni;
    public string name;
    public string lastName;
    public string gender;
    public string birthdate;
};