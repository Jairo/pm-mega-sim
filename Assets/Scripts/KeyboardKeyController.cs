﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Text))]
[RequireComponent(typeof(EventTrigger))]
public class KeyboardKeyController : MonoBehaviour
{
    public OnScreenKeyboardController keyboardController;

    Text _text;

    // Use this for initialization
    void Start ()
    {
        _text = GetComponent<Text>();
        EventTrigger trigger = GetComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry() { eventID = EventTriggerType.PointerClick };

        entry.callback.AddListener((data) => { OnPointerClickDelegate((PointerEventData)data); });
        trigger.triggers.Add(entry);
    }

    public void OnPointerClickDelegate(PointerEventData data)
    {
        if (_text.text.Length > 0)
            keyboardController.SendCharToInput(_text.text[0]);
        else
            keyboardController.Backspace();
    }
}
