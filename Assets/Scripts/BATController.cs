﻿using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class BATController : MonoBehaviour
{
    int quantity;
    int delay;
    IniFile.IniFile iniFile;

    // Use this for initialization
    void Start ()
    {
        iniFile = new IniFile.IniFile(Application.dataPath + "/settings.ini");
        delay = iniFile.GetInteger("BAT", "Delay", 0);
    }

    public void ExecuteBAT()
    {
        StartCoroutine(QueryBATExecution());
    }

    IEnumerator QueryBATExecution()
    {
        for (int i = 0; i < quantity; ++i)
        {
            Process p = new Process();
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            p.StartInfo.FileName = Application.dataPath + "/" + AppModel._ventas[0].productId + ".bat";
            p.Start();
            yield return new WaitForSeconds(delay);
        }
    }

    public void SetLoops(MarketplaceItemController item)
    {
        quantity = item.Amount;
    }
}
