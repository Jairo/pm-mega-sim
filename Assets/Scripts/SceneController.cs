﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
//using DG.Tweening;

public class SceneController : MonoBehaviour
{
    public float switchDuration;

    public Image _image;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnEnable()
    {
        //_image.color = Color.white;
        //_image.DOColor(new Color(1, 1, 1, 0), switchDuration);
    }

    public void ChangeScene(int sceneIndex)
    {
        _image.gameObject.SetActive(true);
        //_image.DOColor(new Color(1, 1, 1, 1), switchDuration);
        StartCoroutine(WaitAndChangeScene(sceneIndex));
    }

    IEnumerator WaitAndChangeScene(int sceneIndex)
    {
        yield return new WaitForSeconds(switchDuration);
        SceneManager.LoadSceneAsync(sceneIndex);
    }
}
