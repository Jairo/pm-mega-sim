﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MarketplaceItemController : MonoBehaviour
{
    public Animator stateMachine;
    public Text priceText;
    public Text unitPriceText;
    public Text amountText;
    public uint maxAmount;

    int _id;
    int _amount = 1;
    float _unitPrice;
    int[] prices;

    public int Id { set { _id = value; } }
    public int Amount { get { return _amount; } }
    public float TotalPrice { get { return _amount * _unitPrice; } }

    // Use this for initialization
    void Start ()
    {
        Refresh();
    }

    void OnEnable()
    {
        _amount = 1;
        Refresh();
    }

    void Refresh()
    {
        priceText.text = (_amount * _unitPrice).ToString("C");
        amountText.text = _amount.ToString();
    }

    public void IncreaseUnit()
    {
        if (_amount < maxAmount)
        {
            _amount++;
            Refresh();
        }
    }

    public void DecreaseUnit()
    {
        if (_amount > 1)
        {
            _amount--;
            Refresh();
        }
    }

    public void SetPrice (string productName)
    {
        IniFile.IniFile iniFile = new IniFile.IniFile(Application.dataPath + "/settings.ini");
        _unitPrice = iniFile.GetInteger("Products", productName);
        unitPriceText.text = "$" + _unitPrice.ToString();
    }

    public void Submit()
    {
        if (_amount > 0)
        {
            AppModel._ventas = new List<Venta>();
            Venta venta = new Venta
            {
                productId = _id,
                quantity = _amount
            };
            AppModel._ventas.Add(venta);

            stateMachine.SetTrigger("nextStep");
        }
    }
}
