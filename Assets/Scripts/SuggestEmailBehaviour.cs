﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(InputField))]
public class SuggestEmailBehaviour : MonoBehaviour
{
    public string[] domains;
    public Text _placeholder;

    InputField _inputField;

    // Use this for initialization
    void Awake()
    {
        _inputField = GetComponent<InputField>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_placeholder != null && domains != null)
        {
            foreach (string domain in domains)
            {
                for (int i = 1; i < domain.Length; ++i)
                {
                    if (_inputField.text.EndsWith(domain.Substring(0, i)))
                    {
                        _placeholder.text = _inputField.text.Substring(0, _inputField.text.IndexOf('@')) + domain;
                        _placeholder.enabled = true;
                        return;
                    }
                }
            }

            _placeholder.enabled = false;
        }
    }

    public void SetSuggestedEmail()
    {
        if (_placeholder != null && domains != null)
        {
            foreach (string domain in domains)
            {
                for (int i = 1; i < domain.Length; ++i)
                {
                    if (_inputField.text.EndsWith(domain.Substring(0, i)))
                    {
                        _inputField.text = _inputField.text.Substring(0, _inputField.text.IndexOf('@')) + domain;
                        _placeholder.enabled = false;
                        return;
                    }
                }
            }
        }
    }
}
