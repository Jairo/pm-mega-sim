﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrandController : MonoBehaviour
{
    public Animator stateMachine;

    public void SetBrand(int id)
    {
        AppModel._user.brandId = id;
        stateMachine.SetTrigger("nextStep");
    }
}
