﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class RegisterManager : MonoBehaviour
{
    public Animator stateMachine;

    public InputField email;
    public InputField provincia;
    public InputField localidad;

    string emailRegex = @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
     + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
                [0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
     + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
                [0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
     + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Register()
    {
        Debug.Log("RegisterManager::Register");
        if (email.text != "" && provincia.text != "" && localidad.text != "")
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(email.text, emailRegex))
            {
                string json;
                json = Resources.Load<TextAsset>("provincies").text;
                Item[] provinciesList = JsonUtility.FromJson<ItemList>(json).List;
                json = Resources.Load<TextAsset>("cities/" + provincia.text).text;
                Item[] citiesList = JsonUtility.FromJson<ItemList>(json).List;

                Item provinceQuery = new List<Item>(provinciesList.Where(a => a.name.Contains(provincia.text)))[0];
                Item cityQuery = new List<Item>(citiesList.Where(a => a.name.Contains(localidad.text)))[0];

                AppModel._user.email = email.text;
                AppModel._user.brandId = 1; // Default value
                AppModel._user.state = provinceQuery.id;
                AppModel._user.city = cityQuery.id;
                AppModel._user.validated = true;
                
                stateMachine.SetTrigger("nextStep");
            }
        }
    }
}
