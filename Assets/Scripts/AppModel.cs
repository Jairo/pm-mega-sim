﻿using System;
using System.Collections.Generic;

[Serializable]
struct Registro
{
    public string userId;
    public string activityId;
    public string touchpointId;
}

[Serializable]
struct Perfil
{
    public string name;
    public string lastName;
    public string gender;
    public string birthdate;
    public long dni;
    public string email;
    //public int state;
    //public int city;
    public string state;
    public string city;
    public int brandId;
    public int rangeage;
    public bool buyOnly;
    public bool validated;
}

[Serializable]
struct Venta
{
    public int productId;
    public int quantity;
}

[Serializable]
static class AppModel
{
    // OPERADOR
    public static Registro _operator;

    // USUARIO
    public static Perfil _user;

    // VENTAS
    public static List<Venta> _ventas;

    public static void Clear()
    {
        _user = new Perfil();
        _ventas = new List<Venta>();
    }
}

[Serializable]
class AppModelInterface
{
    public Registro _operator;
    public Perfil _user;
    public int timestamp;
    public List<Venta> _ventas;

    public AppModelInterface(Registro o, Perfil u, List<Venta> v)
    {
        _operator = o;
        _user = u;
        _ventas = v;
        timestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
    }
}

[Serializable]
class AppModelList
{
    public AppModelInterface[] List;
}